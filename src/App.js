import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Col} from 'react-bootstrap';
import React from 'react';
import MyForm from './components/MyForm';
import MyNavbar from './components/MyNavbar';

class App extends React.Component {

  render() {
    return (
      <Col>
        <MyNavbar /><br />
        <Container>
          <MyForm />
        </Container>
      </Col>
    );
  }
}

export default App;
