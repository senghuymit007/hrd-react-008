import React from 'react';
import {Navbar} from 'react-bootstrap';

export default function MyNavbar() {
    return (
        <>
            <Navbar>
                <Navbar.Brand href="#home">Reactjs Homework 008</Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                        Signed in as: <a href="#login">Senghuy</a>
                    </Navbar.Text>
                </Navbar.Collapse>
            </Navbar>
        </>
    )
}
