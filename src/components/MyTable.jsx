import { Table, Button } from "react-bootstrap";

export default function MyTable({ items,  changeColor, deleteEvent  }) {

  let temp = items.filter(item => {
    return item.id > 0
  })


  return (
    <>
      <Table striped bordered hover size="xm">
        <thead>
          <tr>
            <th>#</th>
            <th>Username</th>
            <th>Gender</th>
            <th>Email</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody className="tableHover">
          {temp.map((item, i) => (
            <tr className={item.selectedRow === i ? "tableSelected" : "" } key={i} onClick={changeColor}>
              <td>{item.id}</td>
              <td>{item.username}</td>
              <td>{item.gender}</td>
              <td>{item.email}</td>
              {/* <Button onClick={} variant="danger">Delete</Button>{' '} */}
              <Button variant="danger" onClick={deleteEvent}>Delete</Button>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  )

}
