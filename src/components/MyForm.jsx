import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Form, Image, Col, Container, Row, Button } from 'react-bootstrap';
import MyTable from './MyTable'

export default class MyForm extends Component {

  constructor(props) {
    super(props);
    this.i = 0;
    this.state = {
      items: [
        {
          id: 0,
          username: "",
          gender: "",
          email: "",
          password: "",

          usernameErr: "",
          genderErr: "",
          emailErr: "",
          passwordErr: "",
          isSignin: false,
        
        }
      ],
    };
  }

  onHandleText = (e) => {
    console.log("e:", e.target);
    //let name = "email"
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        console.log(this.state);

        if (e.target.name === "email") {
          //apple1212@#@gmail.com
          let pattern = /^\S+@\S+\.[a-z]{3}$/g;
          let result = pattern.test(this.state.email.trim());

          if (result) {
            this.setState({
              emailErr: "",
            });
          } else if (this.state.email === "") {
            this.setState({
              emailErr: "Email cannot be empty!",
            });
          } else {
            this.setState({
              emailErr: "Please input the correct email!!!",
            });
          }
        }

        if (e.target.name === "password") {
          let pattern2 =
            /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[_\W])([a-zA-Z0-9_\W]{8,})$/g;
          let result2 = pattern2.test(this.state.password);

          if (result2) {
            this.setState({
              passwordErr: "",
            });
          } else if (this.state.password === "") {
            this.setState({
              passwordErr: "Password cannot be empty!",
            });
          } else {
            this.setState({
              passwordErr: "A password must be 8 charactor up with Capital letter, number, lowercase letter and special Chractor!!!",
            });
          }
        }
      }
    );
  };

  validateBtn = () => {
    if (this.state.emailErr === "" && this.state.passwordErr === "" && this.state.email !== "" && this.password !== "") {
      return false
    } else {
      return true
    }
  };


  //button pass value
  onSubmit = (event) => {
    let userName = this.state.username;
    let gender = this.state.gender;
    let email = this.state.email;
    this.i = this.i + 1;
    const copyArray = Object.assign([], this.state.items); //declare array
    // push data to array
    copyArray.push({
      id: this.i,
      username: userName,
      gender: gender,
      email: email
    });
    this.setState({
      items: copyArray,
    });
    event.preventDefault();
  };

  onToggleActive = i => {
    <tr style={{ background: 'green' }} />
    console.log("Hll");
    //Remove the if statement if you don't want to unselect an already selected item

  };

  // handle remove
  deleteEvent = (id) => {
    const copyArray = Object.assign([], this.state.items);
    copyArray.splice(id, 0);
    this.setState({
      items : copyArray
    })
  }
  render() {
    return (
      <>
        <Row>
          <Col xs="100px">
            <Form onSubmit={this.onSubmit}>
              <Container>
                <Row className="justify-content-md-center">
                  <Col md="auto"> <Image width="100px" height="100px" src="https://lwlies.com/wp-content/uploads/2017/04/avatar-2009.jpg" roundedCircle /></Col>
                </Row>
                <Col><h1>Create Account</h1></Col>
              </Container>
              <Form.Group controlId="formGroupText">
                <Form.Label>Username</Form.Label>

                <Form.Control value={this.state.username}
                 onChange={e => this.setState({ username: e.target.value })} type="text" placeholder="Enter username" />
              </Form.Group>

              <Form.Group>
                <Form.Label>Genter</Form.Label><br />
                <Form.Check
                  custom
                  inline
                  label="Male"
                  type="radio"
                  id="male"
                  name="gender"
                  defaultChecked={this.state.gender === "male" ? true : false}
                  value={"male"}
                  onChange={e => this.setState({ gender: e.target.value })}
                />
                <Form.Check
                  custom
                  inline
                  label="Female"
                  type="radio"
                  id="female"
                  name="gender"
                  defaultChecked={this.state.gender === "female" ? true : false}
                  value={"female"}
                  onChange={e => this.setState({ gender: e.target.value })}

                />
              </Form.Group>

              <Form.Group controlId="formGroupEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control value={this.state.email}
                  onChange={this.onHandleText} name="email" type="email" placeholder="Enter email" />
                <Form.Text className="text-muted">
                  <p style={{ color: "red" }}>{this.state.emailErr}</p>
                </Form.Text>
              </Form.Group>
              <Form.Group controlId="formGroupPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control value={this.state.password}
                  onChange={this.onHandleText} name="password" type="password" placeholder="Password" />
                <Form.Text className="text-muted">
                  <p style={{ color: "red" }}>{this.state.passwordErr}</p>
                </Form.Text>
              </Form.Group>
              <Button disabled={this.validateBtn()} variant="primary" onClick={this.onSubmit}>
                Sign In
              </Button>
            </Form>
          </Col>
          <Col><br />
          {/* {
                this.state.data.map((obj, id) =>
                  <Col key={id} md={3} sm={6}>
                    <MyTable data={obj} delete = {this.deleteEvent.bind(this, id)}  />
                  </Col>
                  )
              } */}
            <MyTable items={this.state.items} deleteEvent = {this.deleteEvent} />
            <Button variant="danger" onClick={this.onDel}>Delete</Button>
          </Col>
        </Row>
      </>
    )
  }
}
